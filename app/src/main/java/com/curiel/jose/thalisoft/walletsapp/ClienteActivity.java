package com.curiel.jose.thalisoft.walletsapp;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by ThaliSoft on 12/06/2018.
 */

public class ClienteActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cliente);
    }
}
